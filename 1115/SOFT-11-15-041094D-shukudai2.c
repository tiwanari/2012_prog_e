// ソフトウェア1 11/15 課題
// 学生証番号: 041094D
// 氏名: 伏見遼平

#include <stdio.h>
#include <stdlib.h>

// パイこね変換を1回行う関数
double single_pie(double x){
	if(x < 0.5){
		return 2 * x;
	}else{
		return 2 - 2 * x;
	}
}

// パイこね変換をn回行う関数
double pie(double x0, int n){
	int i;
	for(i = 0; i < n; i++){
		x0 = single_pie(x0);
		printf("x_%d = %.16f\n", i+1, x0);
	}
	return x0;
}

// usage: ./pie [initial] [iteration]
int main(int argc, char *argv[]){
	double x0 = atof(argv[1]);
	double x = x0;
	int n = atoi(argv[2]);
	printf("n = %d, x_0 = %f\n", n, x0);
	pie(x, n);
}

/*

[宿題3]
10回実行した結果は以下の通り。

$ ./pie 0.1 10
n = 10, x_0 = 0.100000
x_1 = 0.200000
x_2 = 0.400000
x_3 = 0.800000
x_4 = 0.400000
x_5 = 0.800000
x_6 = 0.400000
x_7 = 0.800000
x_8 = 0.400000
x_9 = 0.800000
x_10 = 0.400000

[宿題4]
1000回実行した結果は以下の通り。

$ ./pie 0.1 100
n = 1000, x_0 = 0.100000
x_1 = 0.200000
x_2 = 0.400000
x_3 = 0.800000
x_4 = 0.400000
...
x_35 = 0.800000
x_36 = 0.400000
x_37 = 0.799999
x_38 = 0.400002
x_39 = 0.800003
x_40 = 0.399994
x_41 = 0.799988
x_42 = 0.400024
x_43 = 0.800049
x_44 = 0.399902
x_45 = 0.799805
x_46 = 0.400391
x_47 = 0.800781
x_48 = 0.398438
x_49 = 0.796875
x_50 = 0.406250
x_51 = 0.812500
x_52 = 0.375000
x_53 = 0.750000
x_54 = 0.500000
x_55 = 1.000000
x_56 = 0.000000
x_57 = 0.000000
...
x_99 = 0.000000
x_100 = 0.000000
...
x_998 = 0.000000
x_999 = 0.000000
x_1000 = 0.000000

[宿題5]
double 型は浮動小数点型であり、有効桁数は15桁である。22行目の printf の出力フォーマットを "x_%d = %.16f\n" として、16桁を出力すると以下のようになる。

n = 100, x_0 = 0.100000
x_1 = 0.2000000000000000
x_2 = 0.4000000000000000
x_3 = 0.8000000000000000
x_4 = 0.3999999999999999 // <- ここで誤差が発生している
x_5 = 0.7999999999999998
x_6 = 0.4000000000000004
x_7 = 0.8000000000000007
x_8 = 0.3999999999999986
x_9 = 0.7999999999999972
x_10 = 0.4000000000000057
x_11 = 0.8000000000000114
x_12 = 0.3999999999999773
x_13 = 0.7999999999999545
x_14 = 0.4000000000000909
x_15 = 0.8000000000001819
x_16 = 0.3999999999996362
x_17 = 0.7999999999992724
x_18 = 0.4000000000014552
x_19 = 0.8000000000029104
x_20 = 0.3999999999941792
x_21 = 0.7999999999883585
x_22 = 0.4000000000232831
x_23 = 0.8000000000465661
x_24 = 0.3999999999068677
x_25 = 0.7999999998137355
x_26 = 0.4000000003725290
x_27 = 0.8000000007450581

x_3 = 0.8 ならば、x_4 = 0.4 となるはずだが、0.8 は 2進数で厳密に表現できないため、丸め誤差が発生している。
この誤差は、パイこね変換の中の x*2 という演算により、イテレーションが進むごとに2倍になるため、
どんどん大きくなっていく。結果として、x_55 = 1 となってしまう。

x_42 = 0.4000244140625000
x_43 = 0.8000488281250000
x_44 = 0.3999023437500000
x_45 = 0.7998046875000000
x_46 = 0.4003906250000000
x_47 = 0.8007812500000000
x_48 = 0.3984375000000000
x_49 = 0.7968750000000000
x_50 = 0.4062500000000000
x_51 = 0.8125000000000000
x_52 = 0.3750000000000000
x_53 = 0.7500000000000000
x_54 = 0.5000000000000000
x_55 = 1.0000000000000000

このイテレーションの回数「55」は、doubleの仮数部が52ビットであることに由来する。
x_4 で発生した仮数部の1の誤差がイテレーション1回ごとに2倍になっていき、x_55 で仮数部の最上位ビットに及んでいる。


*/