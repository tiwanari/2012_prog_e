#include <stdio.h>

char *month_name( int n )
{
  static char *name[] = {
    "Illegal month",
    "January", "February", "March", "April",
    "May", "June", "July", "August", "September",
    "October", "November", "December"
  };

  return ( n<1 || n>12 ) ? name[0] : name[n];
}

char *month_name2( int n )
{
  static char name[13][20] = {
    "NG",
    "1st", "2nd", "3rd", "4th",
    "5th", "6th", "7th", "8th", "9th",
    "10th", "11th", "12th"
  };

  return ( n<1 || n>12 ) ? name[0] : name[n];
}

int main( void )
{
//  printf( "%s\n", month_name( 3 ) );
  printf( "%s\n", month_name2( 3 ) );
}

/**
[宿題6]



1. month_name2 は、static を消すと結果がかわる (何も出力しなくなる)

static char name[13][20]; という宣言で、name という名前の 13*20 の2次元配列空間が確保される。
static を付けない場合は、name[n] のメモリ空間は month_name2 関数の外には存在しないので、そこを指すポインタは意味が無い。

2. month_name は、static を消しても結果がかわらない。




*/