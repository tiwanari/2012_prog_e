// ソフトウェア1 11/8 課題
// 学生証番号 041094D
// 氏名 伏見遼平

#include <stdio.h>
#include <string.h>

#define LENBUF  (150000)
#define NofWRD  (25000)
#define NSHOW   (15)

#define HEAD 1
#define NOT_HEAD 0

char allocbuf[LENBUF];
char *allocp = allocbuf;

void bubble( char *wrdptr[], int N )
{
  int i, j;
  char *ptr;

  for(i = 0; i < N-1; i++){
    for(j = N-1; j > i; j--){
      if(*wrdptr[j] < *wrdptr[j-1]){
        ptr = wrdptr[j];
        wrdptr[j] = wrdptr[j-1];
        wrdptr[j-1] = ptr;
      }
    }
  }

  // バブルソートを使って，wrdptr[] を昇順に並び替える
  // Sort wrdptr[] in ascending order using "bubble
  // sort" algorithm.
}

int main( void )
{
  int c, type, in, nwrd, i;
  char *wrdptr[NofWRD];
  type = HEAD;
    
  while((c = getchar()) != EOF){
    if(c == ' ' || c == '\n' || c == '\n'){
      *allocp = '\0';
      type = HEAD;
    }else{
      if(type == HEAD){
        wrdptr[nwrd++] = allocp;
        type = NOT_HEAD;
      }
      *allocp = c;
    }w
    *allocp ++;
  }
  *allocp = '\n';
  

  // news.txt の文字を全て，allocbuf[] にコピーする。但し，ホワイトスペース（単語間の
  // 文字）の場合は，'¥0' を格納する。当然，次に格納する allocbuf[] 内の場所の情報は
  // allocp で管理する。
  // Copy all the characters in news.txt into allocbuf[]. In the case of
  // white-space characers (' ', '¥t', '¥n', and EOF), however, '¥0' should be
  // stored in allocbuf[] instead. allocp should be used as pointer to indicate
  // which memory cell to use for staring a new character data.

  // allocbuf[] に代入する度に，allocp はインクリメントする。
  // Everytime a new data is stored in allocbuf[], allocp has to be incremanted.

  // そして，語頭に相当する文字を allocbuff[] に入れる場合，その場所の allocp を随時
  // wrdptr[] に格納する。
  // When the first character of a new word is stored in allocbuf[], the value
  // of allocp should also be assigned to wrdptr[].

  // 即ち，wrdptr[] は allocbuff[] に入れられる単語群の各単語の先頭アドレスを保有する
  // ようにする。
  // wrdptr[] stores the addresses indicating the first characters of all the
  // words.

  // 単語数を変数 nwrd に格納する。
  // nwrd = the number of words

  // bubble sort 前の単語表示
  printf( "Before sorting\n" );
  for( i = 0 ; i < NSHOW ; i++ ) {
    printf( "%6d : %s\n", i+1, wrdptr[i] );
  }
  printf( "       :\n" );
  printf( "       :\n" );
  for( i = nwrd-NSHOW ; i < nwrd ; i++ ) {
    printf( "%6d : %s\n", i+1, wrdptr[i] );
  }

  bubble( wrdptr, nwrd );

  // bubble sort 後の単語表示
  printf( "\nAfter sorting\n" );
  for( i = 0 ; i < NSHOW ; i++ ) {
    printf( "%6d : %s\n", i+1, wrdptr[i] );
  }
  printf( "       :\n" );
  printf( "       :\n" );
  for( i = nwrd-NSHOW ; i < nwrd ; i++ ) {
    printf( "%6d : %s\n", i+1, wrdptr[i] );
  }

}
