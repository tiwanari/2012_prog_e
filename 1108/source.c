#include <stdio.h>

int main()
{
  char *ptr1  = "Hello World\n";
  char *ptr2  = "Hello Wo\0rld\n";
  char fmt[]  = "str = %s, m = %d\n";
  short *sptr;
  int n, m;
  
  /* 1) */
  printf( ptr1 );

  /* 2) */
  printf( ptr2 );

  /* 3) */
  printf( ptr1+6 );

  /* 4) */
  sptr = (short *)ptr1;
  printf( (char *)(sptr+4) );

  /* 5) */
  printf( "Hello World\n" + 2 );

  /* 6) */
  printf( &("Hello World\n"[4]) + 2 );

  /* 7) */
  printf( &(&("Hello World\n"[3]))[6] );

  /* 8) */
  m = 10;
  printf( fmt + 10, m );

  /* 9) */
  fmt[8] = '\n';
  fmt[9] = '\0';
  printf( fmt, &ptr1[6] );

  /* 10) */
  printf( "n = %d, m = %d\n", 3 < 5, !(4 > 2) );

  return;
}
