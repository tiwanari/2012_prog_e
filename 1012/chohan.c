#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void saikoro(int*, int*);

int main()
{
	long kakekin, motokin;
    int cho_han, me1, me2, result, cont;
    char kekka_str[8];
    
    printf("あなたの所持金は? -->");
    scanf("%ld", &motokin);
    printf("\n");
    while (1) {
        printf("掛け金を入力してください -->");
        scanf("%ld", &kakekin);
        printf("\n表 = 0, 裏 = 1 -->");
        scanf("%d", &cho_han);
        printf("\n");
        saikoro(&me1, &me2);
        result = (me1 + me2) % 2;
        if (result == 0) {
            strcpy(kekka_str, "表");
        }else{
            strcpy(kekka_str, "裏");
        }
        printf("\n 結果は %s でした!", kekka_str);
        if (cho_han == result)
        {
            motokin += kakekin;
        }else{
            motokin -= kakekin;
            if (motokin <= 0L) {
                printf("もうお金がありません...");
                break;
            }
        }
        printf("\n あなたの所持金は%ld円です", motokin);
    }
    return 0;
}

void saikoro(int *a, int *b)
{
    srand((unsigned)time(NULL));//rand()だけではいつも
    *a = rand() % 6 + 1;        //同じパターンになる
    *b = rand() % 6 + 1;        //６で割った余りは０から５
    return;                //サイコロと同じにするため１を足す
}

