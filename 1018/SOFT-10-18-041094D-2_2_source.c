// 10/18 ソフトウェア課題
// 氏名: 伏見遼平
// 学生証番号: 041094D

#include <stdio.h>

#define IN    1
#define OUT   0
#define MAXLENGTH 1024 // max length of word

main()
{
    int i, j, c, nl, nw, nc, wl, maxl, state;
    // i,j: loop counter
    // nl: number of lines
    // nw: number of words
    // nc: number of chars
    // wl: word length of current word
    // maxl: max word length
    
    int wlh[MAXLENGTH];
    // wlh: word length histgram

    state = OUT;
    nl = nw = nc = wl = maxl = 0;
    
    // initialize histgram table
    for (i = 0; i < MAXLENGTH; i++){
        wlh[i] = 0;
    }

    while(( c = getchar() ) != EOF ) {
        ++nc;
        ++wl;
        // increment nc & wl
        if( c == '\n' ){
            ++nl;
        }
        if( c == ' ' || c == '\n' || c == '\t' ){
            if (wl < MAXLENGTH){
                ++wlh[wl-1];
                // increment histgram table
                if (maxl < wl) maxl = wl;
                // set maxl if length of current word is longer
            }
            wl = 0;
            state = OUT;
        }else if( state == OUT ) {
            state = IN;
            ++nw;
        }
    }
    
    // output statistics
    printf( "#line = %d #word = %d #character = %d\n", nl, nw, nc);
    printf( "-----------------------------------------------------\n");
    
    // output table
    for (i = 1; i < maxl+1; i++){
        printf( "%3d : %d\n", i, wlh[i]);
    }
    
    printf( "-----------------------------------------------------\n");
    // output histgram
    for (i = 1; i < maxl+1; i++){
        printf( "%3d | ", i);
        for (j = 0; j < wlh[i] / 50; j++){
            printf("*");
        }
        printf("\n");
    }
}