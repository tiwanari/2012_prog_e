// 10/18 ソフトウェア課題
// 氏名: 伏見遼平
// 学生証番号: 041094D

#include <stdio.h>

#define MAXLEN      100 // 最大の生徒数。ここでは 100とする。
#define THRESHOLD  50.0 // 平均点のしきい値。

main()
{
  int i, maxindex;      // i はループ変数。 maxindex は, データ数を入れておく。
  int max, min, value;  // max, min は, 得点の最大値、最小値。value は, 
  int score[MAXLEN];    // 得点を入れておく配列。
  double average;       // 得点の平均値

  for( i = 0 ; i < MAXLEN ; ++i ) {
    // for 文は、繰り返しを表す制御分である。
    // ループカウンタである i には初期値 0 を代入し、これが MAXLEN まで繰り返す。
    scanf( "%d", &value );     // printf とは逆で，キーボード入力。40[return]という
                               // キー入力をすると，value に 40 という値が代入される。
                               // &value の & はまだ教えてないので，言及しなくてよい。

    if( value < 0 )  break;    // EOF、もしくは負の値が入力されたら、for ループを強引に抜ける
    score[i] = value;          // 配列に得点を収納する
  }
  
  // ここまでで、配列にすべての得点が入った。
  // このときのループカウンタ i の数が、入力された得点の数であり、
  // i から 1 を引くと、配列 score に入っている変数の、最大の添字が得られる (maxindex)。
  maxindex = i-1;

  // 配列にデータが入っているならば、maxindex は 0 以上になっているはずである。
  // 配列にデータが入っていないのに平均値を計算しようとすると、ゼロ徐算でエラーが起きるため、
  // データが入っていることを確認して処理を進める。
  if( maxindex >= 0 ) {   // maxindex は、文字列の最大の 得点が入力されているならば、以下を出力する。
    max = min = score[0]; // 最初の生徒の得点を、暫定の最高点／最低点とする

    average = 0;          // 平均値は、まず合計点を求め、最後に生徒数で割るので、初期値は 0 としておく
    
    for( i = 0 ; i <= maxindex ; ++i ) { // 配列の中のすべての変数について繰り返す。
                                         // score[i] は、今処理している生徒の得点を示す。
                                         
      if( max < score[i] ) // 暫定の最高点よりこの生徒の得点が高ければ、最高点をこの生徒の得点で書き換える。
        max = score[i];

      if( min > score[i] ) // 暫定の最低点よりこの生徒の得点が低ければ、最低点をこの生徒の得点で書き換える。
        min = score[i];
      // これらの処理をすべての生徒について繰り返すと、最後の生徒についての処理が終わった後には
      // 暫定の最高点, 最低点は、全体の最高点, 最低点と一致しているはずである。

      average = average + score[i]; // average にこの生徒の得点を加算する。
      // この処理をすべての生徒について繰り返すと、average には合計点が代入される。
    }

    // 平均値を計算し (average / (maxindex+1)) 、しきい値 (THRESHOLD) と比較する。
    if( ( average = average / (maxindex+1) ) < THRESHOLD ) {
      // しきい値より平均点が低ければ、生徒をけなす文字列を出力する。
      printf( "Students of this class are poor.....\n" );
    }
    // プログラムの計算結果の文字列を出力する。
    printf( "averaged score = %.2lf, min = %d, max = %d, ndata = %d\n", average, min, max, maxindex+1 );
  }
  else {
    // この else は、32行めの if (maxindex >= 0) と対応している。
    // データが入っていない場合は、"no data" を出力して処理を終わる。
    printf( "no data\n" );
  }

}
