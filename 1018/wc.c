#include <stdio.h>

main()
{
    int c, nl;

    nl = 0;
    while( ( c = getchar() ) != EOF )
        if( c == '\n' )
            ++nl;
//
//  == は左右の値が等しいかどうかを比較する比較演算子。
//  if ( c = '\n' ) とすると，nl は最終的に何を表現することになるか？
//  ヒント： = は代入演算子，つまり，c = 'n' は代入式。Ｃは式が値を持つ。
//  だから・・・
//
//  入力された文字（1 byte）が，改行文字（'\n'）であるかどうかをチェック
//  し，その場合だけ，nl を +1 する。
//  'a', 'F', '\n', '\t' '\b' など，１文字を single quatation で囲んだも
//  のは，その文字の ASCII テーブル値を意味する。実際には，'a' の代わり
//  に実際の ASCII コードを数値で指定しても同じであるが，'a' の方がはるか
//  に見やすいのは言うまでもない。
//
//  'e' と "hello! world"，前者は ASCII コード，後者は文字列である。
//  文字列に関しては今後詳細に扱うことになる。
//
//  教科書 p.25 にあるように，例えば，'e' と "e" って，何が違うのでしょう？
//  一文字だけからなる文字列って，それは文字じゃないの？うんにゃ，違います。
//
//  あと，if 文が新しく登場しました。条件分岐です。見れば意図されたことは
//  分かりますが，下記のことに注意。
//  while も for も本体の処理が一文であれば，{ -- } の中括弧は要りませんでした。
//  if 文も同様，本体処理が一文であれば，{ -- } は要りません。
//  複数の文を if 文の処理本体とする場合は，必要になります。
//
//  if ( x == y ) {
//    statement1;
//    statement2;
//  }
//
//  のような感じです。ここで，初心者がよく行うバグを紹介します。
//
//  if ( x == y )
//    statement1;
//    statement2;
//
//  と書くことがあります。この場合，幾ら字下げして整形していたとしても，
//  { -- } が無いので，if の本体は statement1 だけです。これが，条件に
//  よって実行されたり，されなかったりします。statement2 は if 文とは無
//  関係なので，常に実行されます。つまり，
//
//  if ( x == y )
//    statement1;
//
//  statement2;
//
//  ということ。
//
//  結局，プログラマの意図通りに計算機が動かない（つまりバグ），となります。
//  プログラムを書き始めて間もない学生は，for, while, if に対する処理が一文
//  であったとしても，{ -- } を付ける癖を付けておくと良いでしょう。変なバグ
//  に悩まされることが無くなりますよ。
//
    printf( "%d\n", nl );
}

