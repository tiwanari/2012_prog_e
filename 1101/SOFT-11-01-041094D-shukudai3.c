// ソフトウェア1 11/1 課題
// 学生証番号 041094D
// 氏名 伏見遼平

#include <stdio.h>
#include <math.h>
#define NALPHA 26

#define TASK 3
#define DEBUG 0

double log_2(double val){
  return log(val)/log(2);
}

int main()
{
  int count[NALPHA][NALPHA] = {};
  int sum[NALPHA] = {};

  // 条件付き確率
  double prob[NALPHA][NALPHA];
  // 2次の自己情報量
  double ent[NALPHA][NALPHA];
  // 2字の自己情報量の平均
  double avg_ent_s[NALPHA] = {};


  double prob_s[NALPHA];
  double ent_s[NALPHA];
  double avg_ent = 0;

  int c, i, j, k;
  int first, second;

  int total = 0;

  second = ' ';
  while( ( c = getchar() ) != EOF ) {
    first = second;
    second = c;

    if( first - 'a' < NALPHA && first-'a' >= 0 && second-'a' < NALPHA && second-'a' >= 0 ) {
      count[first-'a'][second-'a']++;
      sum[first-'a'] ++;
      total++;
    }
  }

  for( i = 0 ; i < NALPHA ; i++ ) {
    prob_s[i] = (double)sum[i] / total;
    ent_s[i] = -log_2(prob_s[i]);
    avg_ent += ent_s[i] * prob_s[i];

    for( j = 0 ; j < NALPHA ; j++ ) {
      prob[i][j] = (double)count[i][j] / sum[i];
      ent[i][j] = count[i][j] ? -log_2(prob[i][j]) : 0;
      avg_ent_s[i] += ent[i][j] * prob[i][j];
      if(DEBUG) printf( "[%c][%c] = %d\n", 'a'+i, 'a'+j, count[i][j] );
    }
  }

  for( i = 0 ; i < NALPHA ; i++ ) {
    if(DEBUG) printf( "sum [%c] = %d, ent = %f\n", 'a'+i, sum[i], avg_ent_s[i] );
    for( j = 0 ; j < NALPHA ; j++ ) {
      if(DEBUG) printf( "[%c][%c] = %f\n", 'a'+i, 'a'+j, prob[i][j] );
    }
  }

  // [task 1]
  // 集計
  // max, min
  int max_ent_index;
  int min_ent_index;
  double max_ent = 0;
  double min_ent = 1e10;
  for( i = 0 ; i < NALPHA ; i++ ) {
    if(max_ent < avg_ent_s[i]){
      max_ent = avg_ent_s[i];
      max_ent_index = i;
    }
    if(min_ent > avg_ent_s[i]){
      min_ent = avg_ent_s[i];
      min_ent_index = i;
    }
  }

  if(TASK == 1) {
    printf( "max:%c,%.5lf\n", max_ent_index + 'a', max_ent );
    printf( "min:%c,%.5lf\n", min_ent_index + 'a', min_ent );
    return 0; // おわり
  }


  // [task 2]
  // バタチャリア距離の max, min
  double bhat_dist[NALPHA][NALPHA];

  // バタチャリア距離 を求める
  for( i = 0 ; i < NALPHA ; i++ ) {
    for( j = 0 ; j < NALPHA ; j++ ) {
      bhat_dist[i][j] = 0;
      // i, j のバタチャリア距離を求める
      for( k = 0; k < NALPHA ; k++ ) {
        bhat_dist[i][j] += sqrt(prob[i][k] * prob[j][k]);
      }
      bhat_dist[i][j] = bhat_dist[i][j] ? -log(bhat_dist[i][j]) : 0;
      if (DEBUG) printf( "bhat([%c][%c]) = %e\n", 'a'+i, 'a'+j, bhat_dist[i][j] );
    }
  }

  int max_bhat_index_i, max_bhat_index_j, 
      min_bhat_index_i, min_bhat_index_j;
  double max_bhat = 0, min_bhat = 1e10;

  for( i = 0 ; i < NALPHA ; i++ ) {
    for( j = 0 ; j < NALPHA ; j++ ) {
      if( i == j ){
        // bhat_dist of the two same probability distribution
        // ignore
        continue;
      }
      if( max_bhat < bhat_dist[i][j] ){
        max_bhat = bhat_dist[i][j];
        max_bhat_index_i = i;
        max_bhat_index_j = j;
      }
      if( min_bhat > bhat_dist[i][j] ){
        min_bhat = bhat_dist[i][j];
        min_bhat_index_i = i;
        min_bhat_index_j = j;
      }
    }
  }
  if (TASK == 2){ 
    printf( "closest:%c,%c,%.5e\n", min_bhat_index_i+'a', min_bhat_index_j+'a', min_bhat );
    printf( "farest:%c,%c,%.5e\n", max_bhat_index_i+'a', max_bhat_index_j+'a', max_bhat );
    return 0; // おわり
  }

  // [task 3]
  // bhat_dist of the two same probability distribution 
  double bhat;
  for( i = 0 ; i < NALPHA ; i++ ) {
    // bhat_dist of the two same probability distribution should be 0;
    if (TASK == 3) printf("%c:%.5e\n", i+'a', bhat_dist[i][i]);
  }

  if (DEBUG) printf( "average entoropy = %f\n", avg_ent );

  return 0;
}

