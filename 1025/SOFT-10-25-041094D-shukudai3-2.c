// 氏名: 伏見遼平
// 学生証番号: 041094D

// result: 5288
// num of prime: 168

#include <stdio.h>
#include <math.h>

#define MAX 1000
#define PRIME 1
#define NOT_PRIME 0

int main(){
    int i, n, imax, count, num, state;
    count = 0;
    num = 0;
    imax = 0;
    state = PRIME;
    
    for(n = 2; n <= MAX; n++){
        printf("checking if %d is prime or not.\n", n);
        state = PRIME;
        imax = floor(sqrt(n));
        for(i = 2; i <= imax; i++){
            count ++;
            printf("%d %% %d = %d\n", n, i, n % i);
            if(n % i == 0){
                state = NOT_PRIME;
                break;
            }
        }
        if(state){
            printf("%d is prime.\n", n);
            num ++;
        }else{
            printf("%d is not prime.\n", n);
        }
    }
    
    printf("---------------------\n");
    printf("// result: %d\n", count);
    printf("// num of prime: %d\n", num);

    return 0;
}

