// 氏名: 伏見遼平
// 学生証番号: 041094D

// result: 2801
// num of prime: 168

#include <stdio.h>
#include <math.h>

#define MAX 1000
#define PRIME 1
#define NOT_PRIME 0

int main(){
    int i, n, pmax, count, num, state;
    count = 0;
    num = 0;
    state = PRIME;
    pmax = 0;

    int primes[MAX] = {};
    
    for(n = 2; n <= MAX; n++){
        printf("checking if %d is prime or not.\n", n);
        state = PRIME;
        pmax = floor(sqrt(n));
        for(i = 0; i < num; i++){
            if (primes[i] > pmax)break;
            count ++;
            printf("%d %% %d = %d\n", n, primes[i], n % primes[i]);
            if(n % primes[i] == 0){
                state = NOT_PRIME;
                break;
            }
        }
        
        if(state){
            printf("%d is prime.\n", n);
            primes[num] = n;
            num ++;
        }else{
            printf("%d is not prime.\n", n);
        }
    }
    
    printf("---------------------\n");
    printf("// result: %d\n", count);
    printf("// num of prime: %d\n", num);

    return 0;
}

