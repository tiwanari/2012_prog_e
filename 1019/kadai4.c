#include <stdio.h>

int main(){
  int i, c;
  while(c = getchar()){
    if(c == EOF){
      break;
    }else if(i++ % 2 == 0){
      putchar(c);
    }
  }
  return 0;
}

