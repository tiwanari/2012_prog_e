// ソフトウェア演習 11/2 課題
// 学生証番号 041094D
// 氏名 伏見遼平

#include <stdio.h>
int getDays(int, int, int);
int checkLeapYear(int);

static int mon_table[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

int main(){
  //input.
  int year, month, day;
  scanf("%d-%d-%d", &year, &month, &day);

  int totalDays, i;
  totalDays = getDays(year, month, day);
  i = totalDays % 7;

  switch (i){
    case 0:
    printf("%d-%d-%d is a Monday\n", year, month, day);
    break;
    case 1:
    printf("%d-%d-%d is a Tuesday\n", year, month, day);
    break;
    case 2:
    printf("%d-%d-%d is a Wednesday\n", year, month, day);
    break;
    case 3:
    printf("%d-%d-%d is a Thursday\n", year, month, day);
    break;
    case 4:
    printf("%d-%d-%d is a Friday\n", year, month, day);
    break;
    case 5:
    printf("%d-%d-%d is a Saturday\n", year, month, day);
    break;
    case 6:
    printf("%d-%d-%d is a Sunday\n", year, month, day);
    break;
  }
  return 1;
}

int getDays(int year, int month, int day){
  int days = day - 1;

  while(--month > 0){
    if(month == 2 && checkLeapYear(year)){
      days += 29;
    }else{
      days += mon_table[month-1];
    }
  }

  // mod 2000
  // 2001/1/1 and 4001/1/1 are Monday
  year %= 2000;

  while(--year > 0){
    if(checkLeapYear(year)){
      days += 366;
    }else{
      days += 365;
    };
  }
  return days;
}

int checkLeapYear(int year){
  return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

